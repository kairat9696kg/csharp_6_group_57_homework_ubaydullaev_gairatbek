﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Entities
{
    public class Customer: Entity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public ICollection<Order> Orders{ get; set; }
    }
}
