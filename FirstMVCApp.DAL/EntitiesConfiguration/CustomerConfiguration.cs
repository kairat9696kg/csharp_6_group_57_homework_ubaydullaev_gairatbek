﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class CustomerConfiguration : BaseEntityConfiguration<Customer>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Customer> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(p => p.Email)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .HasIndex(b => b.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Customer> builder)
        {
            builder
                .HasMany(b => b.Orders)
                .WithOne(b => b.Customer)
                .HasForeignKey(b => b.CustomerId);
        }
    }
}
