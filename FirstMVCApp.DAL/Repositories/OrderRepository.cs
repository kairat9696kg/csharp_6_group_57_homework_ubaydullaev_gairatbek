﻿using System.Collections.Generic;
using System.Linq;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FirstMVCApp.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Orders;
        }

        public IEnumerable<Order> GetAllWithProduct()
        {
            return entities
                .Include(e => e.Customer)
                .Include(e => e.Product)
                .ThenInclude(p => p.Brand)
                .ToList();
        }
    }
}