﻿using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.Repositories.Contracts;

namespace FirstMVCApp.DAL.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Customers;
        }
    }
}
