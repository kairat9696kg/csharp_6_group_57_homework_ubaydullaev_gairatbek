﻿using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
