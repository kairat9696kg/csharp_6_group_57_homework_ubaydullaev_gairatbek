﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }
}
