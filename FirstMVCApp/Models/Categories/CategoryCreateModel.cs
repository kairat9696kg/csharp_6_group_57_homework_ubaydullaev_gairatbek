﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Categories
{
    public class CategoryCreateModel
    {
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
    }
}
