﻿using System.ComponentModel.DataAnnotations;
using FirstMVCApp.Models.Products;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models.Orders
{
    public class OrderCreateModel
    {
        public ProductModel Product { get; set; }

        [Required]
        public int ProductId { get; set; }

        [Required]
        [Display(Name = "Количество")]
        public int Amount { get; set; }

        [Required]
        [Display(Name = "Заказчик")]
        public int CustomerId { get; set; }

        [Display(Name = "Комментарий")]
        public string Description { get; set; }
        public SelectList CustomersList { get; set; }
    }
}
