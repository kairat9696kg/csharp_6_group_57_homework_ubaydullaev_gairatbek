﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models.Products
{
    public class ProductFilterModel
    {
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        public int? CategoryId { get; set; }

        [Display(Name = "Брэнд")]
        public int? BrandId { get; set; }

        [Display(Name = "Цена от")]
        public decimal? PriceFrom { get; set; }

        [Display(Name = "Цена до")]
        public decimal? PriceTo { get; set; }
        public List<ProductModel> Products { get; set; }
        public SelectList CategoriesSelect { get; set; }
        public SelectList BrandsSelect { get; set; }
    }
}
