﻿using Microsoft.VisualBasic;
using System;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCApp.Models.Customers
{
    public class CustomerCreateModel
    {
        [Required]
        [Display(Name ="Имя Заказчика")]
        public string Name { get; set; }


        [Required]
        [Display(Name = "Email Заказчика")]
        public string Email { get; set; }


        [Required]
        [Display(Name = "Дата рождения Заказчика")]
        public DateTime DateOfBirth { get; set; }
    }
}
