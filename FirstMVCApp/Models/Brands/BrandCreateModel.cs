﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCApp.Models.Brands
{
    public class BrandCreateModel
    {
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        [Display(Name ="Наименование")]
        public string Name { get; set; }
    }
}
