﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.Models.Customers;
using FirstMVCApp.Services.Customers.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CustomerController : Controller
    {
        public ICustomerServise _customerServise;

        public CustomerController(ICustomerServise customerServise)
        {
            if (customerServise == null)
                throw new ArgumentNullException(nameof(customerServise));
            _customerServise = customerServise;
        }

        public IActionResult Index()
        {
            var Customers = _customerServise.GetAllCustomers().ToList(); 
            return View(Customers);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(CustomerCreateModel model)
        {
            _customerServise.CreateCustomer(model);
            return RedirectToAction("Index");
        }
    }
}