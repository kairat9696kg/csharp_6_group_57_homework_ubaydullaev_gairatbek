﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.Models.Brands;
using FirstMVCApp.Services.Brands.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));
            _brandService = brandService;
        }

        public IActionResult Index()
        {           
            try
            {
                var Brands = _brandService.GetAllBrands().ToList();
                return View(Brands);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            try
            {                
                return View();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                return RedirectToAction("Index");
            }
            catch(Exception)
            {
                ViewBag.BadRequestMessage = "Такой Бренд уже существует";
                return View("BadRequest");
            }
           
            
        }
    }
}