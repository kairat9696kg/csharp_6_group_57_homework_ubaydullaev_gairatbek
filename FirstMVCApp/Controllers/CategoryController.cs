﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.Models.Categories;
using FirstMVCApp.Services.Categories.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService )
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));
            _categoryService = categoryService;
        }
        public IActionResult Index()
        {
            try
            {
                var Categories = _categoryService.GetAllCategories();
                return View(Categories);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        public IActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Create(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.BadRequestMessage = "Такой Бренд уже существует";
                return View("BadRequest");
            }


        }
    }
}