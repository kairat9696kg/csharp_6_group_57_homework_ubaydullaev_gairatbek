﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Brands;
using FirstMVCApp.Models.Categories;
using FirstMVCApp.Models.Customers;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Models.Products;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateProductToProductEditModel();
            CreateProductEditModelToProduct();
            CreateOrderCreateModelToOrderMap();
            CreateOrderToOrderIndexModelMap();
            CreateCustomerToCustomerModel();
            CreateCustomerCreateModelToCustomer();
            CreateBrandToBreandModel();
            CreateCategoryToCategoryModel();
            CreateBrandCreateModelToBrand();
            CreateCategoryCreateModelToCategory();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }

        private void CreateProductToProductEditModel()
        {
            CreateMap<Product, ProductEditModel>();
        }

        private void CreateProductEditModelToProduct()
        {
            CreateMap<ProductEditModel, Product>();
        }

        private void CreateOrderCreateModelToOrderMap()
        {
            CreateMap<OrderCreateModel, Order>();
        }

        private void CreateOrderToOrderIndexModelMap()
        {
            CreateMap<Order, OrderIndexModel>()
                .ForMember(target => target.Price,
                src => src.MapFrom(p => p.Product.Price))
                .ForMember(target => target.ProductName,
                    src => src.MapFrom(p => p.Product.BrandId == null ?
                    $"{ProductModel.NoBrand} {p.Product.Name}" : $"{p.Product.Brand.Name} {p.Product.Name}"))
                .ForMember(target => target.OrderCreatedOn,
                    src => src.MapFrom(p => p.CreatedOn))
                .ForMember(target => target.OrderSum,
                    src => src.MapFrom(p => p.Sum))
                .ForMember(target => target.CustomerName,
                    src => src.MapFrom(p => p.Customer.Name));
        }

        private void CreateCustomerToCustomerModel()
        {
            CreateMap<Customer, CustomerModel>();
        }

        private void CreateCustomerCreateModelToCustomer()
        {
            CreateMap<CustomerCreateModel, Customer>();
        }

        private void CreateBrandToBreandModel()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }
        private void CreateCategoryToCategoryModel()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }
        
    }
}
