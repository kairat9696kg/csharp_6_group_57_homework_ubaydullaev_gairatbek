﻿using FirstMVCApp.Models.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Services.Brands.Contracts
{
    public interface IBrandService
    {
        public IEnumerable<BrandModel> GetAllBrands();
        public BrandCreateModel GetBrandCreatModel();
        void CreateBrand(BrandCreateModel model);
    }
}
