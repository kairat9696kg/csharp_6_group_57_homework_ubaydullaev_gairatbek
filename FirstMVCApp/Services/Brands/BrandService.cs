﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Brands;
using FirstMVCApp.Services.Brands.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCApp.Services.Brands
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Brands = unitOfWork.Brands.GetAll().ToList();
                Brand brand = Mapper.Map<Brand>(model);
                if (Brands.Contains(brand))
                    throw new Exception(nameof(brand));
                
                unitOfWork.Brands.Create(brand);
            }
        }

        public IEnumerable<BrandModel> GetAllBrands()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(brands);
                return brandModels;
            }
        }

        public BrandCreateModel GetBrandCreatModel()
        {
            return new BrandCreateModel();
        }

    }
}
