﻿using FirstMVCApp.Models.Customers;
using System.Collections.Generic;

namespace FirstMVCApp.Services.Customers.Contracts
{
    public interface ICustomerServise
    {
        public List<CustomerModel> GetAllCustomers();
        public void CreateCustomer(CustomerCreateModel model);
    }
}
