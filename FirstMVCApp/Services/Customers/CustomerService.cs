﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Customers;
using FirstMVCApp.Services.Customers.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCApp.Services.Customers
{
    public class CustomerService : ICustomerServise
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CustomerService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<CustomerModel> GetAllCustomers()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Customers = unitOfWork.Customer.GetAll().ToList();
                List<CustomerModel> customerModels = Mapper.Map<List<CustomerModel>>(Customers);
                return customerModels;
            }
        }

        public void CreateCustomer(CustomerCreateModel model)
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                Customer customer = Mapper.Map<Customer>(model);
                unitOfWork.Customer.Create(customer);
            }
        }        
    }
}
