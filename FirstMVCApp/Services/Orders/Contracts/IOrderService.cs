﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Orders;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services.Orders.Contracts
{
    public interface IOrderService
    {
        OrderCreateModel GetOrderCreateModel(int productId);
        Order CreateOrder(OrderCreateModel model);
        List<OrderIndexModel> GetOrdersList();
        SelectList GetCustomersSelect();
    }
}
