﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Models.Products;
using FirstMVCApp.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public OrderCreateModel GetOrderCreateModel(int productId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetByIdWithBrandAndCategory(productId);

                var orderCreateModel = new OrderCreateModel()
                {
                    ProductId = productId,
                    CustomersList = GetCustomersSelect(),
                    Product = Mapper.Map<ProductModel>(product)
                };

                return orderCreateModel;
            }
        }

       
        public Order CreateOrder(OrderCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = Mapper.Map<Order>(model);
                order.CreatedOn = DateTime.Now;
                var product = unitOfWork.Products.GetById(order.ProductId);
                order.Sum = order.Amount * product.Price;

                order = unitOfWork.Orders.Create(order);

                return order;
            }
        }

        public SelectList GetCustomersSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var customers = unitOfWork.Customer.GetAll().ToList();
                return new SelectList(customers, nameof(Customer.Id), nameof(Customer.Name));
            }
        }

       

        public List<OrderIndexModel> GetOrdersList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var orders = unitOfWork.Orders.GetAllWithProduct().ToList();

                return Mapper.Map<List<OrderIndexModel>>(orders);
            }
        }
    }
}
