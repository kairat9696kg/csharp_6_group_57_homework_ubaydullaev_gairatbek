﻿using FirstMVCApp.Models.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Services.Categories.Contracts
{
    public interface ICategoryService
    {
        public IEnumerable<CategoryModel> GetAllCategories();
        void CreateCategory(CategoryCreateModel model);
    }
}
