﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Categories;
using FirstMVCApp.Services.Categories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstMVCApp.Services.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public void CreateCategory(CategoryCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                Category category = Mapper.Map<Category>(model);
                if (categories.Contains(category))
                    throw new Exception(nameof(category));

                unitOfWork.Categories.Create(category);
            }
        }

        public IEnumerable<CategoryModel> GetAllCategories()
        {
            using(var unitOfWork = _unitOfWorkFactory.Create())
            {
                var Categories = unitOfWork.Categories.GetAll().ToList();
                List<CategoryModel> categories = Mapper.Map<List<CategoryModel>>(Categories);
                return categories;
            }
        }
    }
}
