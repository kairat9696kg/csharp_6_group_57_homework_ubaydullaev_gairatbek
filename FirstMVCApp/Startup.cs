using System.Globalization;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.EntitiesConfiguration;
using FirstMVCApp.DAL.EntitiesConfiguration.Contracts;
using FirstMVCApp.Services.Brands;
using FirstMVCApp.Services.Brands.Contracts;
using FirstMVCApp.Services.Categories;
using FirstMVCApp.Services.Categories.Contracts;
using FirstMVCApp.Services.Customers;
using FirstMVCApp.Services.Customers.Contracts;
using FirstMVCApp.Services.Orders;
using FirstMVCApp.Services.Orders.Contracts;
using FirstMVCApp.Services.Products;
using FirstMVCApp.Services.Products.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IApplicationDbContextFactory = FirstMVCApp.DAL.IApplicationDbContextFactory;

namespace FirstMVCApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // ����������� �������� - �������� ����������� �������, ����������� ��� ������ ������ ������� (��������, ������������)

            string connectionString = Configuration.GetConnectionString("MainConnectionString");
            
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlServer(connectionString);

            services.AddScoped<IEntityConfigurationsContainer>(sp => new EntityConfigurationsContainer());

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });

            services.AddSingleton<IApplicationDbContextFactory>(
                sp => new ApplicationDbContextFactory(
                    optionsBuilder.Options,
                    new EntityConfigurationsContainer()
                    ));
            
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<ICustomerServise, CustomerService>();
            services.AddSingleton<ICategoryService, CategoryService>();
            services.AddSingleton<IBrandService, BrandService>();

            services.AddControllersWithViews();

            Mapper.Initialize(config => config.AddProfile(new MappingProfile()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            
            var cultureInfo = new CultureInfo("ru-RU");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
